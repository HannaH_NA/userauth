const userSchema = require("../models/register");
const mongoose = require("mongoose");
const User = mongoose.model("Register");

const getUser = {
    getUserList: function (req, res) {
        User.find({})
            .then(result => res.json({ result }),
                console.log("view data"));
    },
    getUser: function (req, res) {
        User.findById({ _id: req.params.id })
            .then(result => res.json({ result }),
                console.log("view data"));
    }

};
module.exports = getUser;
