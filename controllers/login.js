const userSchema = require("../models/register");
const mongoose = require("mongoose");
const User = mongoose.model("Register");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const login = {
    //  login function

    login: function (req, res) {
        var data = { email: "", password: "" };
        data = { ...data, ...req.body };
        User.findOne(
            {
                email: data.email
            },
            function (error, user) {
                if (!error) {
                    if (user != null) {
                        bcrypt.compare(data.password, user.password, (err, result) => {
                            console.log("error ", err, "result ", result);
                            if (!err && result) {
                                const JWTToken = jwt.sign(
                                    {
                                        _id: user._id,
                                    },
                                    "secretKey",
                                    { expiresIn: "10000d" }
                                );
                                res.cookie("test", JWTToken, {
                                    maxAge: 900000,
                                    httpOnly: true
                                });

                                return res.status(200).json({
                                    success: true,
                                    data: {
                                        _id: user._id,
                                        email: user.email,
                                        name: user.name,
                                        token: JWTToken
                                    }
                                });
                            } else {
                                res.status(200).json({
                                    success: false,
                                    error: {
                                        message: [{ password: "Invalid password" }]
                                    }
                                });
                            }
                        });
                    } else {
                        return res.status(200).json({
                            success: false,
                            error: {
                                types: "Validation Error",
                                messages: [{ email: "Email is not registered" }]
                            }
                        });
                    }
                } else {
                    return res.status(200).json({
                        success: false,
                        error: {
                            types: "Failure",
                            messages: "findOne in db failed"
                        }
                    });
                }
            }
        );
    }
};
module.exports = login;
