const registerSchema = require("../models/register");
const mongoose = require("mongoose");
const User = mongoose.model("Register");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const register = {
    register: function (req, res) {
        console.log(req.body);
        var data = { email: "", password: "", name: "" };
        data = { ...data, ...req.body };
        bcrypt.hash(data.password, 10, function (error, hash) {
            if (!error) {
                User.findOne(
                    {
                        email: data.email
                    },
                    function (error, existingUser) {
                        if (!error) {
                            if (existingUser == null) {
                                const myData = new User({
                                    ...data,
                                    password: hash
                                });

                                myData
                                    .save()
                                    .then(response => {
                                        const JWTToken = jwt.sign(
                                            {
                                                _id: response._id,
                                            },
                                            "secretKey",
                                            { expiresIn: "10000d" }
                                        );

                                        console.log("data save to database" + myData);

                                        return res.status(200).json({
                                            success: true,
                                            data: {
                                                _id: response._id,
                                                email: response.email,
                                                password: response.password,
                                                name: response.name,
                                                token: JWTToken
                                            }
                                        });
                                    })
                                    .catch(error => {
                                        return res.status(200).json({
                                            success: false,
                                            error: {
                                                types: "Failure",
                                                messages: "failed adding data to db"
                                            }
                                        });
                                    });
                            } else {
                                return res.status(200).json({
                                    success: false,
                                    error: {
                                        types: "Validation Error",
                                        messages: [{ email: "Email Already Exists" }]
                                    }
                                });
                            }
                        } else {
                            return res.status(200).json({
                                success: false,
                                error: {
                                    types: "Failure",
                                    messages: "findOne in db failed"
                                }
                            });
                        }
                    }
                );
            } else {
                return res.status(200).json({
                    success: false,
                    error: {
                        types: "Failure",
                        messages: "hashing password failed"
                    }
                });
            }
        });
    }
};
module.exports = register;
