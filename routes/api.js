const express = require("express");
const router = express.Router();

const registerController = require("../controllers/register");
const loginController = require("../controllers/login");
const getUserController = require("../controllers/getUser");

router.post("/register", registerController.register);
router.post("/login", loginController.login);
router.get("/getUsers", getUserController.getUserList);
router.get("/getUser/:id", getUserController.getUser);

module.exports = router;