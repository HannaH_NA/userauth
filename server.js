const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const port = 3000;

const api = require("./routes/api");

// Map global promise - get rid of warning
mongoose.Promise = global.Promise;

// Connect to mongoose
mongoose
    .connect("mongodb://localhost/userAuth", {
        //useMongoClient: true
    })
    .then(() => console.log("MongoDB Connected..."))
    .catch(err => console.log(err));

// Body Parser Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/", api);

app.listen(port, () => {
    console.log("Server listening on port " + port);
});
